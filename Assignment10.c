#include<stdio.h>
int gcd(int,int);
int main()
{
int num1,num2;
printf("Enter two integer values:\n");
scanf("%d %d",&num1,&num2);
printf("GCD of %d and %d is = %d",num1,num2,gcd(num1,num2));
return 0;
}
int gcd(int x,int y)
{
if(x==0)
return y;
while(y!=0)
{
if(x>y)
x=x-y;
else
y=y-x;
}
return x;
}