#include<stdio.h>

int myStrlen(const char[]);
int main()
{
  char str[10];
  printf("Enter a string\n");
  scanf("%[^\n]",str);
  int n=myStrlen(str);
  printf("String Length=%d\n",n);
}

int myStrlen(const char str[])
{
  int i;
  for(i=0;str[i]!='\0';i++)
    ;
  return i;
}
