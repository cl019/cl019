#include <stdio.h>
void swap_clval(int, int);
void swap_clref(int *, int *);
int main()
{
    int p=1, q=2, r=3, s=4;
    printf("\n before swap_clval, p = %d and q = %d", p, q);
    swap_clval(p, q);
    printf("\n after swap_clval, p = %d and b = %d", p, q);
    printf("\n\n before swap_clref, r = %d and d = %d", r, s);
    swap_clref(&r, &s);
    printf("\n after swap_clref, r = %d and d = %d", r, s);
    return 0;
}
void swap_clval(int p, int q)
{
    int temp;
    temp = p;
    p = q;
    q = temp;
    printf("\n swapping Call By Value Method a = %d and b = %d", p, q);
}
void swap_clref(int *r, int *s)
{
    int temp;
    temp = *r;
    *r = *s;
    *s = temp;
    printf("\n swapping Call By Reference Method r = %d and s= %d",*r,*s);                  
}